/*-------------------------------------------------------------------------
 *
 * plsample.c - Handler for the PL/Sample
 *              procedural language
 *
 * Portions Copyright (c) 2020, PostgreSQL Global Development Group
 *
 *-------------------------------------------------------------------------
 */

#include <postgres.h>
#include <fmgr.h>

PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(plsample_call_handler);

/*
 * Handle function, procedure, and trigger calls.
 */
Datum
plsample_call_handler(PG_FUNCTION_ARGS)
{
	return 0;
}
