CREATE FUNCTION plsample_call_handler()
RETURNS language_handler
AS 'MODULE_PATHNAME'
LANGUAGE C;

CREATE LANGUAGE plsample
HANDLER plsample_call_handler;

COMMENT ON LANGUAGE plsample IS 'PL/Sample procedural language';
