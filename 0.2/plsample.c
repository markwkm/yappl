/*-------------------------------------------------------------------------
 *
 * plsample.c - Handler for the PL/Sample
 *              procedural language
 *
 * Portions Copyright (c) 2020, PostgreSQL Global Development Group
 *
 *-------------------------------------------------------------------------
 */

#include <postgres.h>
#include <fmgr.h>
#include <funcapi.h>
#include <access/htup_details.h>
#include <catalog/pg_proc.h>
#include <catalog/pg_type.h>
#include <utils/memutils.h>
#include <utils/builtins.h>
#include <utils/syscache.h>

MemoryContext TopMemoryContext = NULL;

PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(plsample_call_handler);

/*
 * Handle function, procedure, and trigger calls.
 */
Datum
plsample_call_handler(PG_FUNCTION_ARGS)
{
	HeapTuple pl_tuple;
	Datum pl_datum;
	const char *source;
	bool isnull;

	/* Fetch the source of the function. */

	pl_tuple = SearchSysCache(PROCOID,
			ObjectIdGetDatum(fcinfo->flinfo->fn_oid), 0, 0, 0);
	if (!HeapTupleIsValid(pl_tuple))
		elog(ERROR, "cache lookup failed for function %u",
				fcinfo->flinfo->fn_oid);

	pl_datum = SysCacheGetAttr(PROCOID, pl_tuple, Anum_pg_proc_prosrc,
			&isnull);
	if (isnull)
		elog(ERROR, "null prosrc");
	ReleaseSysCache(pl_tuple);

	source = DatumGetCString(DirectFunctionCall1(textout, pl_datum));
	elog(LOG, "source text:\n%s", source);

	return 0;
}
