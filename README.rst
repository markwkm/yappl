* 0.1 - Bare minimum code to create a procedural language.
* 0.2 - Retrieve the source text of a function.
* 0.3 - Handle function parameters.
* 0.4 - Return the procedure body.
