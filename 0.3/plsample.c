/*-------------------------------------------------------------------------
 *
 * plsample.c - Handler for the PL/Sample
 *              procedural language
 *
 * Portions Copyright (c) 2020, PostgreSQL Global Development Group
 *
 *-------------------------------------------------------------------------
 */

#include <postgres.h>
#include <fmgr.h>
#include <funcapi.h>
#include <access/htup_details.h>
#include <catalog/pg_proc.h>
#include <catalog/pg_type.h>
#include <utils/memutils.h>
#include <utils/builtins.h>
#include <utils/syscache.h>

MemoryContext TopMemoryContext = NULL;

PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(plsample_call_handler);

/*
 * Handle function, procedure, and trigger calls.
 */
Datum
plsample_call_handler(PG_FUNCTION_ARGS)
{
	HeapTuple pl_tuple;
	Datum pl_datum;
	const char *source;
	bool isnull;

	int i;
	FmgrInfo *arg_out_func;
	Form_pg_type type_struct;
	HeapTuple type_tuple;
	Form_pg_proc pl_struct;
	volatile MemoryContext proc_cxt = NULL;
	Oid *argtypes;
	char **argnames;
	char *argmodes;
	char *value;

	/* Fetch the source of the function. */

	pl_tuple = SearchSysCache(PROCOID,
			ObjectIdGetDatum(fcinfo->flinfo->fn_oid), 0, 0, 0);
	if (!HeapTupleIsValid(pl_tuple))
		elog(ERROR, "cache lookup failed for function %u",
				fcinfo->flinfo->fn_oid);
	pl_struct = (Form_pg_proc) GETSTRUCT(pl_tuple);

	pl_datum = SysCacheGetAttr(PROCOID, pl_tuple, Anum_pg_proc_prosrc,
			&isnull);
	if (isnull)
		elog(ERROR, "null prosrc");
	ReleaseSysCache(pl_tuple);

	source = DatumGetCString(DirectFunctionCall1(textout, pl_datum));
	elog(LOG, "source text:\n%s", source);
		
	arg_out_func = (FmgrInfo *) palloc0(fcinfo->nargs * sizeof(FmgrInfo));
	proc_cxt = AllocSetContextCreate(TopMemoryContext,
			"PL/Sample function", 0, (1 * 1024), (8 * 1024));
	get_func_arg_info(pl_tuple, &argtypes, &argnames, &argmodes);

	/* Iterate through all of the function arguments. */
	elog(LOG, "number of arguments : %d", fcinfo->nargs);
	for (i = 0; i < fcinfo->nargs; i++)
	{
		Oid argtype = pl_struct->proargtypes.values[i];
		type_tuple = SearchSysCache1(TYPEOID, ObjectIdGetDatum(argtype));
		if (!HeapTupleIsValid(type_tuple))
			elog(ERROR, "cache lookup failed for type %u", argtype);

		type_struct = (Form_pg_type) GETSTRUCT(type_tuple);
		fmgr_info_cxt(type_struct->typoutput, &(arg_out_func[i]), proc_cxt);
		ReleaseSysCache(type_tuple);

		value = OutputFunctionCall(&arg_out_func[i], fcinfo->args[i].value);

		elog(LOG, "argument position: %d; name: %s; value: %s", i, argnames[i],
				value);
	}

	return 0;
}
